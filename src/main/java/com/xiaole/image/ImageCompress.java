package com.xiaole.image;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Rotation;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by leven on 2016/11/24.
 */
public class ImageCompress {

    private String inputFile;
    private int standardWidth;
    private int standardHeight;

    private ImageDelegator delegator;

    public ImageCompress(String inputFile, StandardSize standardSize, ImageDelegator delegator) {
        this(inputFile, delegator);
        Size size = this.getBy(standardSize);
        this.standardWidth = size.width;
        this.standardHeight = size.height;
    }

    public ImageCompress(String inputFile, int standardWidth, int standardHeight, ImageDelegator delegator) {
        this(inputFile, delegator);
        this.standardWidth = standardWidth;
        this.standardHeight = standardHeight;
    }

    private ImageCompress(String inputFile, ImageDelegator delegator) {
        this.inputFile = inputFile;
        this.delegator = delegator;
    }

    public void compress() throws IOException, ImageProcessingException, MetadataException {
        // SCALE_SMOOTH的缩略算法：生成缩略图片的平滑度的，优先级比速度高，生成的图片质量比较好，但速度慢
        BufferedImage image = rotate(inputFile);
        Size newSize = resize(image, standardWidth, standardHeight);

        BufferedImage bufferedImage = new BufferedImage(newSize.width, newSize.height, BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().drawImage(image, 0, 0, newSize.width, newSize.height, null); // 绘制缩小后的图

        String outputFile = inputFile.substring(0, inputFile.lastIndexOf(".")) + "_compress.jpg";
        FileOutputStream out = new FileOutputStream(new File(outputFile)); // 输出到文件流
        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out); // 可以正常实现bmp、png、gif转jpg
        encoder.encode(bufferedImage); // JPEG编码
        out.close();

        delegator.notifyRes(outputFile);
    }

    private Size resize(BufferedImage image, int standardWidth, int standardHeight) {
        float oldWidth = image.getWidth(null);
        float oldHeight = image.getHeight(null);

        Size size = new Size();
        if (oldWidth / standardWidth > oldHeight / standardHeight) {
            size.width = standardWidth;
            size.height = (int)(oldHeight * standardWidth / oldWidth);
        }
        else {
            size.height = standardHeight;
            size.width = (int)(oldWidth * standardHeight / oldHeight);
        }
        return size;
    }

    private BufferedImage rotate(String imagePath) throws ImageProcessingException, IOException, MetadataException {
        File imageFile = new File(imagePath);
        BufferedImage originalImage = ImageIO.read(imageFile);

        Metadata metadata = ImageMetadataReader.readMetadata(imageFile);
        ExifIFD0Directory exifIFD0Directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
        if (exifIFD0Directory == null) {
            return originalImage;
        }

        int orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);

        switch (orientation) {
            case 1:
            case 2: // Up.
                break;
            case 3:
            case 4: // Down.
                originalImage = Scalr.rotate(originalImage, Rotation.FLIP_VERT);
                break;
            case 5:
            case 6: // Left.
                originalImage = Scalr.rotate(originalImage, Rotation.CW_90);
                break;
            case 7:
            case 8: // Right.
                originalImage = Scalr.rotate(originalImage, Rotation.CW_270);
                break;
            default:
                break;
        }

        return originalImage;
    }

    private static class Size {
        public int width;
        public int height;

        public Size() {

        }

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    public enum StandardSize {
        SMALL, MEDIUM, LARGE
    }

    public static Size getBy(StandardSize size) {
        switch (size) {
            case SMALL:
                return new Size(720, 480);
            case MEDIUM:
                return new Size(1280, 720);
            case LARGE:
                return new Size(1920, 1080);
            default:
                return new Size(1280, 720);
        }
    }

}
