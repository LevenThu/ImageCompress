package com.xiaole.image;

/**
 * Created by leven on 2016/11/25.
 */
public interface ImageDelegator {

    void notifyRes(String outFilePath);
}
