package com.xiaole.image;

import com.xiaole.image.ImageCompress.StandardSize;

import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.MetadataException;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by leven on 2016/11/24.
 */
public class ImageService {
    
    private static int corePoolSize = 2;
    private static int maximumPoolSize = 16;
    private static int keepAliveTime = 30;
    private static int queueSize = 32;
    
    private static ThreadPoolExecutor threadPool;
    
    static {
        threadPool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueSize), new ThreadPoolExecutor.AbortPolicy());
    }
    
    public static void addTask(String fileName, int standWidth, int standardHeight, ImageDelegator from) {
        threadPool.execute(new TestRunnable(fileName, standWidth, standardHeight, from));
    }

    public static void addTask(String fileName, StandardSize size, ImageDelegator from) {
        threadPool.execute(new TestRunnable(fileName, size, from));
    }

    static class TestRunnable implements Runnable {

        private ImageCompress compress;

        public TestRunnable(String fileName, int standardWidth, int standardHeight, ImageDelegator from) {
            compress = new ImageCompress(fileName, standardWidth, standardHeight, from);
        }

        public TestRunnable(String fileName, ImageCompress.StandardSize size, ImageDelegator from) {
            compress = new ImageCompress(fileName, size, from);
        }
        
        public void run() {
            try {
                compress.compress();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ImageProcessingException e) {
                e.printStackTrace();
            } catch (MetadataException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void main(String[] args) {
        String[] files = new String[] { "height.JPG", "width.JPG" };
        for (String file : files) {
            addTask(file, StandardSize.MEDIUM, new ImageDefaultNotifer());
        }
    }
}
